# Godot Push Pull

A simple example project showing a KinematicBody2D able
to push and pull a box.

License: MIT / CC0 / Public Domain
