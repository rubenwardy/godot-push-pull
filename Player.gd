extends KinematicBody2D

const speed = 200
const jumpForce = 200
const gravity = 800

var velocity: Vector2 = Vector2()
var hasDoubleJumped = false
var pulling: RigidBody2D = null
var pullingOffset = Vector2()

onready var sprite = $Sprite
onready var interactZone = $InteractZone


signal respawned


func update_input(delta):
	var friction = 0.6 if is_on_floor() else 0.05
	var walk = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")

	if pulling:
		walk *= 0.25

	velocity.x = lerp(velocity.x, walk * speed, friction)

	if abs(walk) > 0.01 and not pulling:
		set_facedir(walk > 0)

	if Input.is_action_just_pressed("jump") and pulling == null:
		if is_on_floor():
			velocity.y -= jumpForce
			hasDoubleJumped = false
		elif not hasDoubleJumped:
			hasDoubleJumped = true
			velocity.y = min(-jumpForce * 0.8, velocity.y)


func _physics_process(delta):
	update_input(delta)
	velocity = move_and_slide(velocity, Vector2.UP, false, 4, 0.785398, false)
	velocity.y += gravity * delta

	for i in get_slide_count():
		var collision = get_slide_collision(i)
		var dot = collision.normal.dot(Vector2(1, 0))

		# Push rigid bodies
		if collision.collider is RigidBody2D:
			var body = collision.collider as RigidBody2D
			body.apply_central_impulse(-collision.normal * velocity.length() * 1)

	# Respawn
	if position.y > 200:
		emit_signal("respawned")


func set_facedir(is_right):
	sprite.flip_h = is_right
	interactZone.scale.x = -1 if is_right else 1


func _unhandled_input(event):
	if event.is_action_released("interact"):
		if pulling and is_instance_valid(pulling):
			pulling.detach()
			pulling = null
			return

		var areas = interactZone.get_overlapping_areas()
		for area in areas:
			if area.has_method("interact"):
				area.interact(self)

		var bodies = interactZone.get_overlapping_bodies()
		for body in bodies:
			if body.has_method("attach"):
				body.attach(self)
				pulling = body


func _on_respawned():
	pulling = null
	position.x = 0
	position.y = 0
